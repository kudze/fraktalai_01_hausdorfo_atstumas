# Hausdorfo atstumas

Ši programa apskaičiuoja ir nubrežia hausdorfo atstumą tarp 2 aibių.

## Priklausomybės

1) PHP 8.3
2) PHP-GD pletinys.

## Paleidimas

```
php hausdorf.php pav_01.png pav01out.png
```