<?php

if ($argc !== 3) {
    echo "Naudojimas: hausdorf.php <input_file> <output_file>\n";
    return;
}

$inputFilePath = $argv[1];
$outputFilePath = $argv[2];

if (!is_file($inputFilePath)) {
    echo "$inputFilePath does not exist!\n";
    return;
}

if (is_file($outputFilePath)) {
    echo "$outputFilePath already exists!\n";
    return;
}

//Open image.
$img = imagecreatefrompng($inputFilePath);
if (!$img)
    return;

//Read non transparent pixels from $img
function readPixelGroups(GdImage $img): array
{
    $A = [];
    $B = [];

    $A_col = null;
    $B_col = null;

    $width = imagesx($img);
    $height = imagesy($img);

    for ($x = 0; $x < $width; $x++) {
        for ($y = 0; $y < $height; $y++) {
            $rgbaCol = imagecolorat($img, $x, $y);
            ['alpha' => $a] = imagecolorsforindex($img, $rgbaCol);

            if ($a === 127)
                continue;

            if ($A_col === null) {
                $A_col = $rgbaCol;
                $A[] = ['x' => $x, 'y' => $y];
                continue;
            }

            if ($A_col === $rgbaCol) {
                $A[] = ['x' => $x, 'y' => $y];
                continue;
            }

            if ($B_col === null) {
                $B_col = $rgbaCol;
                $B[] = ['x' => $x, 'y' => $y];
                continue;
            }

            if ($B_col === $rgbaCol) {
                $B[] = ['x' => $x, 'y' => $y];
                continue;
            }

            throw new RuntimeException("There are more than two colors in file");
        }
    }

    if(empty($A))
        throw new RuntimeException("There are no colors in image!");

    if(empty($B))
        throw new RuntimeException("There is only one color in image!");

    return [$A, $B];
}

[$A, $B] = readPixelGroups($img);

$countA = count($A);
$countB = count($B);
echo "There is $countA pixels in A array\n";
echo "There is $countB pixels in B array\n";

//Hausdorf distance calc now
//2d distance
function dist2d(array $a, array $b): float
{
    return sqrt(pow($a['x'] - $b['x'], 2) + pow($a['y'] - $b['y'], 2));
}


//Atstumas nuo taško iki aibės.
function d1(array $a, array $B, ?array &$bOut = null): float
{
    $res = INF;

    foreach ($B as $b) {
        $dist = dist2d($a, $b);
        if ($dist >= $res)
            continue;

        $res = $dist;
        $bOut = $b;
    }

    return $res;
}

//Atstumas nuo aibės iki aibės.
function d2(array $A, array $B, ?array &$aOut = null, ?array &$bOut = null): float
{
    $res = 0;

    foreach ($A as $a) {
        $_bOut = [];
        $dist = d1($a, $B, $_bOut);

        if ($dist <= $res)
            continue;

        $res = $dist;
        $aOut = $a;
        $bOut = $_bOut;
    }

    return $res;
}

//Hausfordo atsumas nuo aibės iki aibės
function d3(array $A, array $B, ?array &$aOut = null, ?array &$bOut = null): float
{
    $aOutFromA = [];
    $bOutFromA = [];
    $aOutFromB = [];
    $bOutFromB = [];

    $distFromA = d2($A, $B, $aOutFromA, $bOutFromA);
    $distFromB = d2($B, $A, $aOutFromB, $bOutFromB);

    if($distFromA > $distFromB)
    {
        $aOut = $aOutFromA;
        $bOut = $bOutFromA;
        return $distFromA;
    }

    $aOut = $aOutFromB;
    $bOut = $bOutFromB;
    return $distFromB;
}

$hausdorfStart = [];
$hausdorfEnd = [];
$hausdorfDistance = d3($A, $B, $hausdorfStart, $hausdorfEnd);

echo "Hausdorff distance: $hausdorfDistance px\n";
echo "Hausdorff start: {x = {$hausdorfStart['x']}, y = {$hausdorfStart['y']}}\n";
echo "Hausdorff start: {x = {$hausdorfEnd['x']}, y = {$hausdorfEnd['y']}}\n";

//Piešiam linija
imageline(
    $img,
    $hausdorfStart['x'],
    $hausdorfStart['y'],
    $hausdorfEnd['x'],
    $hausdorfEnd['y'],
    imagecolorallocate($img, 255, 0, 0)
);

//Išsaugom
imagesavealpha($img, true);
imagepng($img, $outputFilePath);
imagedestroy($img);